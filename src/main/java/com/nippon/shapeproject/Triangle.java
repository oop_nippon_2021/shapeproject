/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.shapeproject;

/**
 *
 * @author Nippon
 */
public class Triangle {
    private double h;
    private double b;
    public static final double A = 0.5;
    
    public Triangle (double h,double b){
        this.h = h;
        this.b = b;
    }
    public double calArea(){
        return A*b*h;
    }
    public double getA(){
        return h;
    }
    public double getB(){
        return b;
    }
    public void setC(double h, double b){
         if(b<=0 ){
            System.out.println("Error : Redius must more than 0");
            return;
            
        }else if(h<=0){
             System.out.println("Error : Redius must more than 0");
            return;
         }
         this.b =b;
         this.h =h;
    }
    
    
}
