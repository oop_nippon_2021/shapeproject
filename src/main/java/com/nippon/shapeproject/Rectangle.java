/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.shapeproject;

/**
 *
 * @author Nippon
 */
public class Rectangle {
     private double w;
     private double h;
     public Rectangle(double w,double h){
         this.w =w;
         this.h = h;
     }
     public double calArea(){
         return w*h;
     }
     public double getA(){
        return w;
    }
     public double getB(){
         return h;
    }
     public void setR(double w,double h){
          if(w<=0 ){
            System.out.println("Error : Redius must more than 0");
            return;
            
        }else if(h<=0){
             System.out.println("Error : Redius must more than 0");
            return;
         }
         this.w =w;
         this.h =h;
     }
     
}
