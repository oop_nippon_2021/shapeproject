/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.shapeproject;

/**
 *
 * @author Nippon
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(3,2);
        System.out.println("Area of rectangle1  is " + rectangle1.calArea());
        rectangle1.setR(3.1, 4.3);
        System.out.println("Area of rectangle1  is " + rectangle1.calArea());
        rectangle1.setR(0,0);
        System.out.println("Area of rectangle1  is " + rectangle1.calArea());
        rectangle1.setR(0.1,3);
        System.out.println("Area of rectangle1  is " + rectangle1.calArea());
    }
}
